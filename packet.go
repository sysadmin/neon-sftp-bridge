// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2022 Harald Sitter <sitter@kde.org>

package main

import (
	"github.com/packethost/packngo"
)

func packetsList(c *packngo.Client) ([]string, error) {
	addresses := []string{}

	projects, _, err := c.Projects.List(nil)
	if err != nil {
		return addresses, err
	}

	for _, project := range projects {
		listOpt := &packngo.ListOptions{}
		devices, _, err := c.Devices.List(project.ID, listOpt)
		if err != nil {
			return addresses, err
		}
		for _, device := range devices {
			for _, network := range device.Network {
				addresses = append(addresses, network.Address)
			}
		}
	}

	return addresses, nil
}

func packetsPoll() ([]string, error) {
	addresses := []string{}
	for _, token := range conf.PacketAccessTokens {
		list, err := packetsList(packngo.NewClientWithAuth("packngo lib", token, nil))
		if err != nil {
			return addresses, err
		}

		addresses = append(addresses, list...)
	}

	return addresses, nil
}
