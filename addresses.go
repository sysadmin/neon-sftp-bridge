// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2019 Harald Sitter <sitter@kde.org>

package main

import (
	"fmt"
	"sync"
	"time"
)

// Could/should use atomic really. the manual mutex playing is meh
var _addresses []string
var _addressesMutex sync.RWMutex

// GetAddresses returns a slice of strings which are known and trusted
// addresses. Remotes which do not match the addresses in the slice are
// considered foreign and shouldn't be talked to.
func GetAddresses() []string {
	// NB: we don't need to manually copy this or anything. The slice is fixed
	// once it is assigned to _addresses, we only need the mutex to ensure the
	// var doesn't get assigned while we read it. Once we have the slice
	// we are good to go.
	_addressesMutex.RLock()
	ret := _addresses
	_addressesMutex.RUnlock()
	return ret
}

func addressReset() {
	_addressesMutex.Lock()
	_addresses = []string{}
	_addressesMutex.Unlock()
}

func addressPoll() {
	addresses := []string{}

	// If there was an error, enter startup state. Errors mustn't be fatal
	// to prevent breakage in production.
	// This is particularly important in case the API endpoints go down for
	// whatever reason.

	list, err := dropletsPoll()
	if err != nil {
		fmt.Println(err)
		addressReset()
		return
	}
	addresses = append(addresses, list...)

	list, err = packetsPoll()
	addresses = append(addresses, list...)
	if err != nil {
		fmt.Println(err)
		addressReset()
		return
	}

	_addressesMutex.Lock()
	_addresses = addresses
	_addressesMutex.Unlock()
}

func addressesPollTick() {
	// This would cause 360 polls an hour, we have a limit of 5000 requests per
	// hour on the DO API side, so we should be well within the limit here.
	pollTicker := time.NewTicker(10 * time.Second)
	go func() {
		for {
			addressPoll()
			<-pollTicker.C
		}
	}()
}
