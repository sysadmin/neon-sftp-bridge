<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
-->

# Deployment

This runs on charlotte. Run deploy.sh on the server.

WARNING: for access control restriction the configs `pangea-digital-ocean.yaml`
and `pangea-packet.yaml` need to be available in `~/.config` and contain API
tokens to query the respective cloud APIs.
