// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2016-2020 Harald Sitter <sitter@kde.org>

package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"

	"github.com/coreos/go-systemd/activation"
	"github.com/gin-gonic/gin"
	"github.com/pkg/sftp"

	"net/http"
)

func isHidden(fName string) bool {
	return strings.HasPrefix(fName, ".")
}

func getFile(c *gin.Context, sftp *sftp.Client, path string) {
	fmt.Println("file")
	file, err := sftp.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	buffer := bufio.NewReader(file)
	// For unknown reasons reading from sftp files never EOFs, so
	// we need to manually keep track of how much we can and have read and abort
	// once all bytes are read.
	stat, err := file.Stat()
	if err != nil {
		panic(err)
	}
	toRead := stat.Size()
	c.Stream(func(w io.Writer) bool {
		wrote, err := buffer.WriteTo(w)
		toRead -= wrote
		if err != nil || toRead <= 0 {
			return false
		}
		return true
	})
}

func getDir(c *gin.Context, sftp *sftp.Client, path string) {
	fmt.Println("dir")
	fileInfos, err := sftp.ReadDir(path)
	if err != nil {
		panic(err)
	}
	var buffer bytes.Buffer
	buffer.WriteString("<html>")
	for _, info := range fileInfos {
		url := info.Name()
		if isHidden(url) {
			continue
		}
		buffer.WriteString(fmt.Sprintf("<a href='%s'>%s</a><br/>\n", url, url))
	}
	buffer.WriteString("</html>")
	c.Data(http.StatusOK, "text/html", buffer.Bytes())
}

func newSession() (*ssh.Client, *sftp.Client) {
	// key, err := ioutil.ReadFile(filepath.Join(os.Getenv("HOME"), ".ssh/keys/kde-8192"))
	key, err := ioutil.ReadFile(filepath.Join(os.Getenv("HOME"), ".ssh/id_rsa"))
	if err != nil {
		log.Fatalf("unable to read private key: %v", err)
	}

	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	hostKeyCallback, err := knownhosts.New(filepath.Join(os.Getenv("HOME"), ".ssh/known_hosts"))
	if err != nil {
		log.Fatalf("failed to create known_hosts handler: %v", err)
	}

	config := &ssh.ClientConfig{
		User: "ftpneon",
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		// https://cyruslab.net/2020/10/23/golang-how-to-write-ssh-hostkeycallback/ is of use
		// Also note https://github.com/golang/go/issues/29286
		HostKeyCallback: hostKeyCallback,
	}

	client, err := ssh.Dial("tcp", "rsync.kde.org:22", config)
	if err != nil {
		log.Fatalf("unable to connect: %v", err)
	}

	sftp, err := sftp.NewClient(client)
	if err != nil {
		log.Fatal(err)
	}

	return client, sftp
}

func knownIP(c *gin.Context) bool {
	ip := c.ClientIP()
	// 1.7+ doesn't handle X-Forwarded-For automatically and 1.7.3 (latest at the time)
	// doesn't have the sufficient API to set trusted proxies. Manually handle stuff for now.
	// https://github.com/gin-gonic/gin/pull/2692
	if ip == "127.0.0.1" { // Trusted fronting proxy
		clientIP := c.GetHeader("X-Forwarded-For")
		clientIP = strings.TrimSpace(strings.Split(clientIP, ",")[0])
		if clientIP == "" {
			clientIP = strings.TrimSpace(c.GetHeader("X-Real-Ip"))
		}
		if clientIP != "" {
			ip = clientIP
		} else {
			return true // localhost + not proxying = always allowed
		}
	}
	knownAddresses := GetAddresses()
	if len(knownAddresses) <= 0 {
		// Allow by default to not impair production services should the IP
		// listing break at some point.
		return true
	}
	res := false
	for _, addr := range knownAddresses {
		if !net.ParseIP(ip).Equal(net.ParseIP(addr)) {
			continue
		}
		res = true
		break
	}
	if !res {
		// Log the checked IP and all server IPs to allow investigating after the
		// fact if this should go wrong at some point.
		fmt.Printf("Stranger danger: %s not in %s\n", ip, knownAddresses)
	}
	return res
}

func allowed(c *gin.Context) bool {
	path := path.Clean(c.Param("path"))
	fmt.Println(path)
	return !strings.HasPrefix(path, "/.") &&
		(strings.HasPrefix(path, "/stable") || strings.HasPrefix(path, "/unstable"))
}

func get(c *gin.Context) {
	if path.Clean(c.Param("path")) == "/robots.txt" {
		c.String(http.StatusOK, "User-agent: *\nDisallow: /")
		return
	}

	if !knownIP(c) {
		c.String(http.StatusForbidden, "STRANGER DANGER! Only trusted other servers are allowed!")
		return
	}

	if !allowed(c) {
		c.String(http.StatusForbidden, "not an allowed path")
		return
	}

	path := "/home/ftpneon/" + c.Param("path")

	ssh, sftp := newSession()
	defer ssh.Close()
	defer sftp.Close()

	fileInfo, err := sftp.Stat(path)
	if err != nil {
		c.String(http.StatusNotFound, err.Error())
	}

	if fileInfo.IsDir() {
		getDir(c, sftp, path)
	} else {
		getFile(c, sftp, path)
	}
}

func main() {
	addressesPollTick()

	router := gin.Default()
	router.GET("*path", get)

	listeners, err := activation.Listeners()
	if err != nil {
		panic(err)
	}

	log.Println("starting servers")
	var servers []*http.Server
	for _, listener := range listeners {
		server := &http.Server{Handler: router}
		go server.Serve(listener)
		servers = append(servers, server)
	}

	if len(servers) == 0 {
		panic("no systemd listeners set")
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// Wait for some quit cause.
	// This could be INT, TERM, QUIT
	// We'll then do a zero downtime shutdown.
	// This relies on systemd managing the socket and us doing graceful listener
	// shutdown. Once we are no longer listening, the system starts backlogging
	// the socket until we get restarted and listen again.
	// Ideally this results in zero dropped connections.
	<-quit
	log.Println("servers are shutting down")

	for _, srv := range servers {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalf("Server Shutdown: %s", err)
		}
	}

	log.Println("Server exiting")
}
